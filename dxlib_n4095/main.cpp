﻿#include <DxLib.h>
#include <chrono>
#include <thread>
#include <type_traits>
#include <stdexcept>
#include <string>
//https://github.com/LoliGothick/Cranberries/blob/40415947d70be1b192419cac673c08925918645e/cranberries/type_traits.hpp
namespace cranberries {
	template < typename ...Dummy >
	using void_t = void;

	enum class return_any {};

	template <class, class R = void, class = void> struct is_callable : std::false_type {};

	template <class Fn, class... ArgTypes, class R>
	struct is_callable<Fn(ArgTypes...), R, cranberries::void_t<decltype(std::declval<Fn>()(std::declval<ArgTypes>()...))>>
		: std::is_convertible<decltype(std::declval<Fn>()(std::declval<ArgTypes>()...)), R> {};

	template <class Fn, class... ArgTypes>
	struct is_callable<Fn(ArgTypes...), return_any, cranberries::void_t<decltype(std::declval<Fn>()(std::declval<ArgTypes>()...))>>
		: std::true_type {};
};
namespace detail {
	bool normal_con_f() {
		bool re = -1 != ProcessMessage() && 0 == ScreenFlip() && 0 == ClearDrawScreen();
		if (!re) throw std::runtime_error("ProcessMessage() return -1.");
		return re;
	}
}
template<
	typename Rep, typename Period,
	typename Func,
	//! Required concept
	//! Func: (std::size_t count, double fps) -> any
	//!   i    : current loop counter number will be passed.
	//!   total: 2nd argument of animation_cfr_sync will be passed.
	std::enable_if_t<cranberries::is_callable<Func(std::size_t, std::size_t), cranberries::return_any>::value, std::nullptr_t> = nullptr
>
void animation_cfr_sync(const std::chrono::duration<Rep, Period>& time, std::size_t count, Func&& f)
{
	namespace ch = std::chrono;
	using time_point = typename ch::high_resolution_clock::time_point;
	using duration = typename ch::high_resolution_clock::time_point::duration;
	const duration totoal_time = ch::duration_cast<duration>(time);
	//1frameあたりに使える時間
	const auto frame_per_time = totoal_time / count;
	time_point begin = ch::high_resolution_clock::now();
	const time_point before_frame = begin;
	for (std::size_t i = 0; detail::normal_con_f() && ((begin - before_frame) < totoal_time); ++i, begin = ch::high_resolution_clock::now()) {
		//実行
		f(i, count);

		const auto end = ch::high_resolution_clock::now();
		const duration d = end - begin;
		//fの実行時間が1frameあたりに使える時間を超える場合はframeをdropする
		if (frame_per_time < d) {
			if ((2 * frame_per_time) < d) {
				//1frame dropする時
				++i;
			}
			else {
				const auto tmp = static_cast<std::size_t>(
					//frame_per_time < d, 0 < frame_per_time よりこのキャストは安全
					(d - frame_per_time) / frame_per_time
				);
				i += tmp - 1;
			}
		}
		else {
			//Wait
			std::this_thread::sleep_until(begin + frame_per_time);
		}
	}
}
template<
	typename Rep, typename Period,
	typename Func,
	//! Required concept
	//! Func: (std::chrono::nanoseconds duration) -> any
	//!   duration  : duration since loop begin
	//!   total_time: 1st argument of animation_vfr_sync will be passed.
	std::enable_if_t<cranberries::is_callable<Func(std::chrono::nanoseconds, std::chrono::nanoseconds), cranberries::return_any>::value, std::nullptr_t> = nullptr
>
void animation_vfr_sync(const std::chrono::duration<Rep, Period>& time, Func&& f)
{
	namespace ch = std::chrono;
	using namespace std::chrono_literals;
	const auto before_loop = ch::high_resolution_clock::now();
	const auto total = ch::duration_cast<typename ch::high_resolution_clock::duration>(time);
	for (auto current = before_loop; detail::normal_con_f() && (current - before_loop) < total; current = ch::high_resolution_clock::now()) {
		f(ch::duration_cast<std::chrono::nanoseconds>(current - before_loop), total);
		//負荷軽減
		std::this_thread::sleep_for(20ns);
	}
}
int init(int width, int height) {
	SetMainWindowText(_T("DxLib animation test"));
	SetOutApplicationLogValidFlag(false);
	SetGraphMode(width, height, 16);
	ChangeWindowMode(true);
	SetBackgroundColor(255, 255, 255);
	if (DxLib_Init() == -1)return -1;// エラーが起きたら直ちに終了
									 //SetTransColor(0, 0, 254);
	SetDrawScreen(DX_SCREEN_BACK);
	ClearDrawScreen();
	return 0;
}
class dxlib_finally {
public:
	dxlib_finally() = default;
	dxlib_finally(const dxlib_finally&) = delete;
	dxlib_finally(dxlib_finally&&) = delete;
	dxlib_finally& operator=(const dxlib_finally&) = delete;
	dxlib_finally& operator=(dxlib_finally&&) = delete;
	~dxlib_finally() {
		DxLib::DxLib_End();
	}
};
int WINAPI WinMain(HINSTANCE, HINSTANCE, LPSTR, int)
{
	using namespace std::chrono_literals;
	using namespace std::string_literals;
	constexpr int window_width = 1000;
	constexpr int window_height = 667;
	if (-1 == init(window_width, window_height)) return -1;
	dxlib_finally dxlib_end;

	try {
		//添付の絵素材は https://www.pakutaso.com/20150142021post-5083.html より
		const auto graph = DxLib::LoadGraph(_T("bsHIRO92_tukinomieruoka.jpg"));
		const auto font = DxLib::CreateFontToHandle(nullptr, 22, 3, DX_FONTTYPE_ANTIALIASING);
		const auto yellow = DxLib::GetColor(255, 255, 0);

		//15秒間に255回描画する
		animation_cfr_sync(15s, 255, [graph, font, yellow, window_width](std::size_t i, std::size_t total) {
			const int b = static_cast<int>(i * 255 / total);
			DxLib::SetDrawBright(b, b, b);
			DxLib::DrawGraph(0, 0, graph, false);
			DxLib::SetDrawBright(255, 255, 255);
			DxLib::DrawStringToHandle(0, 0, _T("CFR(constant frame rate) mode"), yellow, font);
			const int pos = static_cast<int>(i * window_width / total);
			DxLib::DrawBox(0, 32, pos, 45, yellow, true);
		});

		std::this_thread::sleep_for(2s);

		animation_vfr_sync(15s, [graph, font, yellow, window_width](std::chrono::nanoseconds current, std::chrono::nanoseconds total) {
			const int b = static_cast<int>(current * 255 / total);
			DxLib::SetDrawBright(b, b, b);
			DxLib::DrawGraph(0, 0, graph, false);
			DxLib::SetDrawBright(255, 255, 255);
			DxLib::DrawStringToHandle(0, 0, _T("VFR(variadic frame rate) mode"), yellow, font);
			const int pos = static_cast<int>(current * window_width / total);
			DxLib::DrawBox(0, 32, pos, 45, yellow, true);
		});

		std::this_thread::sleep_for(2s);
	}
	catch (...) {
		return -1;
	}
	return 0;
}
